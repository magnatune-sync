use std::env;
use std::error::Error;
use std::io;
use std::io::BufReader;
use std::path::{Path, PathBuf};
use rss::Channel;
use log::{warn, info, debug, trace};
use reqwest::{Client, RequestBuilder, Url, IntoUrl, header};
use std::fs;
use std::fs::OpenOptions;
use rusqlite::Connection;
use zip::ZipArchive;
use simplelog::{TermLogger, LevelFilter};
use structopt::StructOpt;
use directories::ProjectDirs;

#[derive(Debug)]
struct MagnatuneClient {
    base_url: Url,
    username: String,
    password: String,
    client: Client,
}

impl MagnatuneClient {
    fn new(host: String, username: String, password: String) -> Result<MagnatuneClient, Box<Error>> {
        let mut headers = header::HeaderMap::new();
        headers.insert(header::USER_AGENT, header::HeaderValue::from_static(concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"))));
        let client = Client::builder().default_headers(headers).build()?;

        Ok(MagnatuneClient {
            base_url: Url::parse(&format!("http://{}", host))?,
            username: username,
            password: password,
            client: client,
        })
    }

    fn url(&self, path: &[&str]) -> Result<Url, Box<Error>> {
        let base = Url::options().base_url(Some(&self.base_url));
        let mut url = base.parse("")?;
        url.path_segments_mut().map_err(|_| "cannot be base")?.extend(path);

        Ok(url)
    }

    fn get<U: IntoUrl>(&self, url: U) -> RequestBuilder {
        self.client
            .get(url)
            .basic_auth(self.username.clone(), Some(self.password.clone()))
    }
}

#[derive(Debug)]
struct Album {
    name: String,
    artist: String,
    sku: String,
    songs: Vec<Song>,
}

impl Album {
    fn path(&self) -> PathBuf {
        [ &self.artist, &self.name ].iter().collect()
    }
}

#[derive(Debug)]
struct Song {
    name: String,
    track_no: i32,
}

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(long = "magnatune-host", default_value = "download.magnatune.com")]
    magnatune_host: String,

    /// Show verbose output
    #[structopt(short = "v", long = "verbose")]
    verbose: bool,

    /// Show debug output
    #[structopt(short = "d", long = "debug")]
    debug: bool,

    /// Target directory
    #[structopt(parse(from_os_str))]
    folder: PathBuf,

    /// Limit to the first N entries in RSS feed. Useful for testing
    #[structopt(long = "limit")]
    limit: Option<usize>,
}

fn main() {
    let opt = Opt::from_args();
    ::std::process::exit(match run(opt) {
       Ok(_) => 0,
       Err(err) => {
           eprintln!("{}", err);
           1
       }
    });
}

fn run(opt: Opt) -> Result<(), Box<std::error::Error>> {
    let level = if opt.debug {
        LevelFilter::Trace
    }
    else if opt.verbose {
        LevelFilter::Info
    } else {
        LevelFilter::Warn
    };
    TermLogger::init(level, simplelog::Config::default())?;

    let username = env::var("magnatune_username").map_err(|_| "The environment variable magnatune_username must be set. Suggestion: run read magnatune_username; export magnatune_username")?;
    let password = env::var("magnatune_password").map_err(|_| "The environment variable magnatune_password must be set. Suggestion: run read -s magnatune_password; export magnatune_password")?;

    let magnatune_sqlite_url = "http://he3.magnatune.com/info/sqlite_normalized.db.gz";

    info!("Syncing database...");
    let sqlite_file = sync_magnatune_db(magnatune_sqlite_url).unwrap();

    let magnatune = MagnatuneClient::new(opt.magnatune_host, username, password).unwrap();

    info!("Fetching favourites...");
    let favourites = fetch_favourites(&magnatune, &sqlite_file, opt.limit)?;

    let favourites = favourites.into_iter().filter_map(|r| {
        match r {
            Ok(f) => Some(f),
            Err(e) => {
                info!("Problematic favourites RSS entry: {}", e);
                None
            }
        }
    });
    trace!("{:#?}",favourites);

    info!("Looking for missing albums...");
    let mut missing_locally = Vec::new();
    for album in favourites {
        if check_album(&album, &opt.folder) {
            trace!("OK: {} - {}", album.artist, album.name);
        }
        else {
            info!("Missing: {} - {}", album.artist, album.name);
            missing_locally.push(album);
        }
    }

    if missing_locally.len() > 0 {
        info!("Downloading...");

        for album in missing_locally {
            download(&magnatune, &album, &opt.folder)?;
        }
    }
    else {
        info!("Nothing missing. My work is done.");
    }

    Ok(())
}

fn fetch_favourites(client: &MagnatuneClient, sqlite_file: &Path, limit: Option<usize>) -> Result<Vec<Result<Album, Box<Error>>>, Box<Error>> {
    let db = Connection::open(sqlite_file)?;

    let mut url = client.url(&["member","favorites_export"])?;
    url.query_pairs_mut().append_pair("format", "rss");
    let res = client.get(url).send()?.error_for_status()?;

    let reader = BufReader::new(res);
    let channel = Channel::read_from(reader)?;

    let iter = channel.items().into_iter();
    let iter: Box<Iterator<Item = _>> = if let Some(n) = limit {
        Box::new(iter.take(n))
    }
    else {
        Box::new(iter)
    };

    let albums = iter.map(|item| {
        if let Some(l) = item.link() {
            find_album(&db, l)
        }
        else {
            return Err(From::from(format!("RSS entry without link: {:#?}", item)));
        }
    }).collect();

    Ok(albums)
}

fn sync_magnatune_db(url: &str ) -> Result<PathBuf, Box<Error>> {
    let project_dirs = ProjectDirs::from("org.cakebox", "", env!("CARGO_PKG_NAME"))
        .ok_or_else(|| format!("Could not find project directory"))?;
    let dir = project_dirs.cache_dir();

    fs::create_dir_all(dir)?;

    let filename = dir.join("magnatune.sqlite");
    let attr = fs::metadata(&filename);
    if attr.is_err() {//TODO: check age
        info!("Magnatune database missing or outdated, downloading...");
        download_magnatune_db(url, &filename).map_err(|e| format!("Error downloading magnatune database: {}", e))?;
    }

    Ok(filename)
}

fn download_magnatune_db(url: &str, filename: &Path) -> Result<(), Box<Error>> {
    let client = Client::new();

    let res = client.get(url).send()?.error_for_status()?;

    let mut uncompressed_res = flate2::bufread::GzDecoder::new(BufReader::new(res));

    let mut file = OpenOptions::new().write(true)
        .create_new(true)
        .open(filename)?;
    io::copy(&mut uncompressed_res, &mut file)?;

    Ok(())
}

fn find_album(db: &Connection, link: &str) -> Result<Album, Box<Error>> {
    let url = Url::parse(link)?;
    let path: Vec<&str> = url.path_segments().unwrap().collect();

    let parse_error = Err(From::from(format!("Expected url in artists/albums/:sku format: {}", link)));
    if path.len() != 3 { return parse_error }
    if path[0] != "artists" { return parse_error }
    if path[1] != "albums" { return parse_error }

    let sku = path[2];

    let album = lookup_sku_in_db(&db, &sku)?;

    Ok(album)
}

fn lookup_sku_in_db(db: &Connection, sku: &str) -> Result<Album, Box<Error>> {
    let mut album_stmt = db.prepare_cached(
        "SELECT album_id, albums.name, artists.name AS artist, albums.sku FROM albums JOIN artists ON (artist_id=artists_id) WHERE sku = ?"
    )?;
    let mut rows = album_stmt.query_and_then(&[&sku.to_string()], |album_row| {
        let album_id: i64 = album_row.get(0);
        let mut song_stmt = db.prepare_cached(
            "SELECT name, track_no FROM songs WHERE album_id = ?"
        )?;
        let songs_iter = song_stmt.query_map(&[&album_id], |song_row| {
            Song {
                name: song_row.get(0),
                track_no: song_row.get(1),
            }
        })?;
        let mut songs: Vec<Song> = Vec::new();
        for s in songs_iter {
            songs.push(s?);
        }

        Ok(Album {
            name: album_row.get(1),
            artist: album_row.get(2),
            sku: album_row.get(3),
            songs: songs,
        })
    })?;

    if let Some(album_result) = rows.next() {
        album_result
    }
    else {
        Err(From::from(format!("Could not find album with SKU {} in database", sku)))
    }
}

//TODO: also check that all songs are present
fn check_album(album: &Album, folder: &Path) -> bool {
    let stat = folder.join(album.path()).metadata();
    stat.is_ok()
}

fn download(client: &MagnatuneClient, album: &Album, folder: &Path) -> Result<(), Box<Error>> {
    info!("Downloading {}", album.name);

    let album_folder = folder.join(album.path());
    fs::create_dir_all(&album_folder)?;

    download_and_unpack_flac(client, album, folder)?;

    if let Err(e) = download_file(client, &["music", &album.artist, &album.name, "cover.jpg"], &album_folder.join("cover.jpg")) {
        warn!("Error downloading cover: {}", e);
    }
    if let Err(e) = download_file(client, &["music", &album.artist, &album.name, "artwork.pdf"], &album_folder.join("artwork.pdf")) {
        warn!("Error downloading artwork: {}", e);
    }

    Ok(())
}

fn download_and_unpack_flac(client: &MagnatuneClient, album: &Album, folder: &Path) -> Result<(), Box<Error>> {
    // "http:/download.magnatune.com/music/".urlenc(artist)."/".urlenc(album)."/sku-wav.zip -- you won't need an API key.
    let filename = format!("{}-flac.zip", album.sku);
    let zip_path = folder.join(&filename);
    download_file(client, &["music", &album.artist, &album.name, &filename], &zip_path)?;

    unpack(&zip_path, &album, &folder)?;

    fs::remove_file(zip_path)?;

    Ok(())
}

fn download_file(client: &MagnatuneClient, url_path: &[&str], target: &Path) -> Result<(), Box<Error>> {
    let mut resp = client.get(client.url(url_path)?).send()?.error_for_status()?;

    let mut file = fs::OpenOptions::new().write(true).create_new(true).open(&target)?;
    resp.copy_to(&mut file)?;

    Ok(())
}

fn unpack(zip_path: &Path, album: &Album, folder: &Path) -> Result<(), Box<Error>> {
    let album_folder = folder.join(album.path());
    debug!("Unpacking {:?} to {:?}", zip_path, album_folder);//FIXME

    let rdr = fs::File::open(zip_path)?;
    let mut zip = ZipArchive::new(rdr)?;

    for i in 0..zip.len() {
        let mut file = zip.by_index(i)?;

        info!("Extracting {:?}", file.sanitized_name());//FIXME

        let mut relative_path = file.sanitized_name();
        if relative_path.starts_with(&album.artist) {
            relative_path = relative_path.strip_prefix(&album.artist).unwrap().into();
        }
        if relative_path.starts_with(&album.name) {
            relative_path = relative_path.strip_prefix(&album.name).unwrap().into();
        }

        let path = album_folder.join(relative_path);

        if ! path.parent().unwrap().canonicalize()?.starts_with(album_folder.canonicalize()?) {
            return Err(format!("zip file wanted to write outside containing folder: {:?} is not beneath {:?}", path, folder).into()) //FIXME
        }

        debug!("Extracting to {:?}", path);//FIXME
        let mut wtr = fs::OpenOptions::new().write(true).create_new(true).open(path)?;
        io::copy(&mut file, &mut wtr)?;
    }

    Ok(())
}
